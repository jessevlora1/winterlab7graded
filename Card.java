public class Card{
	
	private String suit;							//Fields
	private String value;
	private Double rank;
	
	public Card(String suit, String value){			//Constructor
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){						//getter
		return this.suit;
	}
	
	public String getValue(){						//getter
		return this.value;
	}
	
	public String toString(){						//TtoString
		return this.suit + " " + "of" + " "+ this.value;
	}
	
	public double calculateScore(){
		double score = 0.0;
		
		String[] values =  {"Ace","2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
		for(int i = 0; i < values.length; i++){
			if(values[i] == this.value){
				score += i + 1.0;
			}	
			
		}
		
		
		String[] suits = {"Hearts", "Spades", "Diamonds", "Clubs"};
				if(suits[0] == this.suit){
					score += 0.4;
				}
				if(suits[1] == this.suit){
					score += 0.3;
				}
				if(suits[2] == this.suit){
					score += 0.2;
				}
				if(suits[3] == this.suit){
					score += 0.1;
				}
				return score;

		}	
}
	