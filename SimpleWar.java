public class SimpleWar{
	
	public static void main (String[] args){
	
		Deck gameDeck = new Deck();
		gameDeck.shuffle();
		//System.out.println(gameDeck);
		
		int player1 = 0;
		int player2 = 0;
		
		//System.out.println(gameDeck.drawTopCard());
		//System.out.println(gameDeck.drawTopCard());
	
		while(gameDeck.length() > 1){

		Card card1 = gameDeck.drawTopCard();
		Card card2 = gameDeck.drawTopCard();
	
		System.out.println("Player 1 score is: " + player1);
		System.out.println("Player 2 score is: " + player2);

		System.out.println("The rank of: " + card1 + " is: " + card1.calculateScore());
		System.out.println("The rank of: " + card2 + " is: " + card2.calculateScore());
		
		if(card1.calculateScore() > card2.calculateScore()){
			System.out.println("Card 1 won!");
			player1 += 1;
		}
		if(card2.calculateScore() > card1.calculateScore()){
			System.out.println("Card 2 won!");
			player2 += 1;
		}
		System.out.println("Player 1 score is: " + player1);
		System.out.println("Player 2 score is: " + player2);
		}
		if(player1 > player2){
			System.out.println("Player 1 won");
		}
		if(player2 > player1){
			System.out.println("Player 2 won");
		}
	}
	
}